package jtm.activity06;

public class Martian implements Humanoid, Alien, Cloneable {
	int weight;
	int birthweight;
	int item;
	Object eaten;
	
	public Martian() {
		weight = -1;
	}
	public Martian(Martian martian) {
		this.weight = Alien.BirthWeight;
	}

	@Override
	public void eat(Object item) {
		if (!(this.eaten==null))
			return;
		
		eaten = item;
		if (item instanceof Human) {
			Humanoid tmp = (Humanoid)item;
			weight = weight + tmp.getWeight();
		}
		if (item instanceof Martian) {
			Martian tmp = (Martian) item;
			weight = weight + tmp.getWeight();
			
		}
		if (item instanceof Integer) {
			Integer tmp = (Integer) item;
			weight = weight + tmp;
		}
	}

	@Override
	public void eat(Integer food) {
		if (eaten instanceof Integer && (Integer) this.eaten !=0){
			return;
		}
		if (eaten == null) {
			eaten = food;
			weight = weight + (int) eaten;
		}
	}

	@Override
	public Object vomit() {
		// TODO Auto-generated method stub
		if (eaten instanceof Integer && (Integer) this.eaten !=0){	
		Integer copyInt = (Integer) this.eaten;
		this.weight -= (Integer) this.eaten;
		
		this.eaten = null;
		
		this.weight=(Integer) this.eaten;
		return copyInt;
		}else if (eaten instanceof Integer) {
		return this.weight;
	}
		if (eaten == null)
			return null;
		else
			weight = -1;
		
		Object copy = this.eaten;
		this.eaten = null;
		return copy;
	}

	@Override
	public String isAlive() {
		// TODO Auto-generated method stub
		return "I AM IMMORTAL!";
	}

	@Override
	public String killHimself() {
		// TODO Auto-generated method stub
		return isAlive();
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return weight;
	}

     public Object clone() throws CloneNotSupportedException {
    	 return this.clone(this);
     }
    private Object clone (Object current) {
    	if (current instanceof Integer) {
    		return current;
    	}
    
	 if(current instanceof Martian) {
		 Martian currentMartian = (Martian)current;
		 Martian newMartian = new Martian();
		 newMartian.eaten = clone(currentMartian.eaten);
		 newMartian.weight = currentMartian.weight;
//		 newMartian.getWeight();
		 return newMartian;
		 
	 }
    	 if (current instanceof Human) {
    		 Human currentHuman = (Human) current;
    		 Human newHuman = new Human();
    		 newHuman.setEaten(currentHuman.getEaten());
//    		 newHuman.weight(currentHuman.getWeight());
    		 return newHuman;
    	 }
    	 else return null; 
     }

@Override
public String toString() {
	return getClass().getSimpleName() + ": " + weight + " [" + eaten + "] ";
}
public static void main (String[] args) throws CloneNotSupportedException {
	
	Martian m1 = new Martian();
	Martian m2 = new Martian();
	Martian m3 = new Martian();
	Human h1 = new Human();
    m1.eat(m2);
    m3.eat(h1);

	System.out.println(m1.vomit());
	
	System.out.println(m3);
	System.out.println(m3.clone());
	
}
}
