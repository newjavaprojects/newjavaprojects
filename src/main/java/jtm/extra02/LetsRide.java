package jtm.extra02;

/**
 * 
 * This class represents a bus driving in route, stopping at bus stops and
 * adding passengers.
 */
public class LetsRide {
	int busStopCount = 5;// bus stop count
	int passengersAtStart = 10;// passengers at the start in bus
	int passengersCount = 0;// overall passengers count in bus
	int seatsCount = 100;// bus seats count

	public LetsRide(int busStopCount, int passengersInStop, int seatsCount) {
		// TODO #1: Set passed values to LetsRide object
		
	}

	public int passengersAtRouteEnd() {
		// TODO #2: Calculate how many passengers will be in bus at the end of
		// route. Overall passenger count
		// is incremented by bus stop number. Example: In bus stop No.1
		// passenger count will be increased by 1, in stop No.2 it
		// will be increased by 2 and so on until bus reaches route end.
		// Note: Overall passenger count can't exceed seat count
		
	    for (int i = 1; i < busStopCount+1; i++) {
	        passengersCount = passengersAtStart + (passengersAtStart+i);
	        //was 10+ 1(at 1st stop)+2(at 2nd stop)+3(at 3rd stop)+4(at 4th stop)+5(at 5th stop)
	    }
        System.out.println(passengersCount);
        
        if (passengersCount>seatsCount){
            System.out.println("Bus is full! not possible to sit");
            
		return passengersCount;
	}

	public int freeSeats() {
		int freeSeats = 0;
		// TODO #3: Calculate how much seats are free in bus
		
	    freeSeats = seatsCount-passengersCount;
	    System.out.println(freeSeats);
	    
		return freeSeats;
	}

	public boolean isFull() {
		boolean status = false;
		// TODO #4: Check if bus is full.
		if (!(passengersCount == seatsCount)) {
			System.out.println("The bus is not full!");
		}
		else {
			System.out.println("The bus is full!");
		}
		return status;
	}

	public int getBusStopCount() {
		return busStopCount;
	}

	public void setBusStopCount(int busStopCount) {
		this.busStopCount = busStopCount;
	}

	public int getPassengersAtStart() {
		return passengersAtStart;
	}

	public void setPassengersAtStart(int passengersAtStart) {
		this.passengersAtStart = passengersAtStart;
	}

	public int getPassengersCount() {
		return passengersCount;
	}

	public void setPassengersCount(int passengersCount) {
		this.passengersCount = passengersCount;
	}

	public int getSeatsCount() {
		return seatsCount;
	}

	public void setSeatsCount(int seatsCount) {
		this.seatsCount = seatsCount;
	}

}
