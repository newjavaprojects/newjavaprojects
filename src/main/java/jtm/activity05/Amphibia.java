package jtm.activity05;

import jtm.activity04.Road;	

public class Amphibia extends Vehicle {

	byte sails;	
	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize, wheels);
		this.sails = sails;
}
@Override
public String move(Road road) {
if (road instanceof WaterRoad)
	return Ship.move(road, this);
//
//	return super.getId() + " " + getType() + " is sailing on " + road;
//			
////		"Cannot drive on " + road;
else 
//	String status = super.move(road);
//	if (status.contains("moving"))
		return super.move(road).replace("Vehicle", "Amphibia");
}
public static void main(String[]args) {
	Amphibia testAmp = new Amphibia ("5", 2.5f, 50, (byte)4, 5);
	System.out.println(testAmp);
}
}
