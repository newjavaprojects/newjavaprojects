package jtm.extra04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/*-
 * 
 * This class represents string tokenizer exercise.
 */
public class StringTokenizerExercise {
	public StringTokenizer splitString(String text, String delimiter) {
		String[] list = null;
		  StringTokenizer st = new StringTokenizer(text, delimiter);
		    StringBuffer sb = new StringBuffer();

		    while (st.hasMoreTokens()) {
		        String s = st.nextToken();
		        if (s.equals(delimiter)) {
		            sb.append(", ").append(delimiter);
		        } else {
		            sb.append(s).append(delimiter);
		            if (st.hasMoreTokens())
		                st.nextToken();
		    }
		    return (new StringTokenizer(sb.toString(), delimiter));
		}
		// TODO # 1 Split passed text by given delimiter and return array with
		// split strings.
		// HINT: Use System.out.println to better understand split method's
		// functionality.
		return list;
	}

	public List<String> tokenizeString(String text, String delimiter) {
		// TODO # 2 Tokenize passed text by given delimiter and return list with
		// tokenized strings.
		String line = null;
		String fileIn  = "students.txt";
		List<String> studentList = new ArrayList<>();

			    FileReader fileReader = new FileReader(fileIn);
			    BufferedReader bufferedReader = new BufferedReader(fileReader);
				
			    while ((line = bufferedReader.readLine()) != null) {
				String[] msg = line.split(",");
				int ID = Integer.parseInt(msg[0]);
				String firstName = msg[1];
				String lastName = msg[2];
				int phoneNumber = Integer.parseInt(msg[3]);
				studentList.add(new Student(ID, firstName, lastName, phoneNumber));
			}
			    bufferedReader.close();

         return studentList;
	}

	public StringBuilder createFromFile(String filepath, String delimiter) {
		File students = new File(filepath);
		List<Student> list = new ArrayList<Student>();
		BufferedReader in = null;
		
		 StringBuilder stringBuilder = new StringBuilder();

		    try{
			    FileReader fileReader = new FileReader(fileIn);
			    BufferedReader bufferedReader = new BufferedReader(fileReader);
		        String line = null;

		        while((line = bufferedReader.readLine()) != null){
		            stringBuilder.append(line);
		        }

		    }catch(Exception e){
		        System.out.println("error");
		    }

		    return stringBuilder;
		}}
		// TODO # 3 Implement method which reads data from file and creates
		// Student objects with that data. Each line from file contains data for
		// 1 Student object.
		// Add students to list and return the list. Assume that all passed data
		// and
		// files are correct and in proper form.
		// Advice: Explore StringTokenizer or String split options.
		//return list;
	