package jtm.extra04;

public class Student {
	private int ID;
	private String firstName;
	private String lastName;
	private int phoneNumber;
	
	public Student() {
		ID = 1;
		firstName = "John";
		lastName = "Doe";
		phoneNumber = 27173829;
		
	}
		public Student(int ID1, String firstName1, String lastName1, int phoneNumber1) {
	
			ID = ID1;
			firstName = firstName1;
			lastName = lastName1;
			phoneNumber = phoneNumber1;
					
		}		
		public int getID() {
			return ID;
		}
		
		public void setID(int ID) {
			this.ID=ID;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName =firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName =lastName;
		}
			public int getPhoneNumber() {
				return phoneNumber;
			}
			
			public void setPhoneNumber(int phoneNumber) {
				this.phoneNumber=phoneNumber;
			}

		public static void main(String[] args) {
			Student student= new Student();
		
			System.out.println("Student's id: " + student.getID()+ "; Student's first name: " + student.getFirstName()+ "; Student's last name: " + student.getLastName()+ "; Student's phone: " + student.getPhoneNumber());
	       
			Student student1= new Student(2, "Jane", "Doer", 26125126);
			
			System.out.println("Student's id: " + student1.getID()+ "; Student's first name: " + student1.getFirstName()+ "; Student's last name: " + student1.getLastName()+ "; Student's phone: " + student1.getPhoneNumber());
		       
			Student student2= new Student(3, "Rob", "Wick", 27183498);
			
			System.out.println("Student's id: " + student2.getID()+ "; Student's first name: " + student2.getFirstName()+ "; Student's last name: " + student2.getLastName()+ "; Student's phone: " + student2.getPhoneNumber());
		       

	}}
			
		
		// TODO #1 Create new student, assign him ID, first name, last name and
		// phone number from passed values

