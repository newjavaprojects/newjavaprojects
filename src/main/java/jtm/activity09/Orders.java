package jtm.activity09;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Consumer;


/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in List.sort() method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */

public class Orders<T> implements Iterator<Order>{
	int index = 0;
	List<Order> orders;
	private int order; // number of elements in order
	private int current; // current position in the iteration
	private T[ ] items; // items in the order
		
	public Orders(){
		index = -1;
		orders = new ArrayList<>();
	}

	public void ArrayIterator (T[ ] collection, int size) {
		items = collection;
		order = size;
		current = 0;
		}
	
	@Override
    public String toString() {
        return "ItemName: \"+ itemName + \"OrdererName: \"+ customer + \" Count: \" + count";
	}
	public void add(Order item){
		orders.add(item);
	}
	public List<Order> getItemsList(){
		return orders;
	}
   public Set<Order> getItemsSet(){

		Set<Order> orderSet = new HashSet<>();
		Iterator<Order> iterator = orders.iterator();
		 
		while (iterator.hasNext()) {
		    Order number = iterator.next();
		    System.out.println(number);
		}
		return orderSet;
	}
    @Override
	public void forEachRemaining(Consumer<? super Order> o) {
		//  Auto-generated method stub
		
	}

	@Override
	public boolean hasNext() {
//	    if (orders.size() > 0) {
//	        return true;
//	       
//	      }
		return (current < order);
		//return false;
	}


	@Override
	public Order next() {
		//  Auto-generated method stub
		if (! hasNext( ))
			throw new NoSuchElementException( );
			current++;
			return (Order) items[current - 1];
//
//		return null;
	}

	@Override
	public void remove() {

//		orders.remove(next());
		//  Auto-generated method stub
		Iterator<Order> iterator = orders.iterator();
		while (iterator.hasNext()) 
        { 
            Order x = (Order)iterator.next(); 
            if (order < 10) 
                iterator.remove(); 
        } 
	}

	public void sort() {
		//  Auto-generated method stub
		
		Collections.sort(orders);
}
//		<T> boolean isSorted(Iterable<? extends T> iterable,
//	            Comparator<? super T> comparator) {
//	boolean beforeFirst = true;
//	T previous = null;
//	for (final T current : iterable) {
//	if (beforeFirst) {
//	   beforeFirst = false;
//	   previous = current;
//	   continue;
//	}
//	if (comparator.compare(previous, current) > 0) {
//	   return false;
//	}
//	previous = current;
//	}
//	return true;
//	}
	}

	/*-
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */

