package jtm.activity04;

public class TrafficManagementSystem{

	// TODO Auto-generated constructor stub
static Transport[] transports;
static Road[] roads;

/**
 * This method is called to set up TransportManagementSystem
 * @param roads
 * @param transports
 */	
public static void initSystem(int roads, int transports) {
	addRoads(roads);
	addTransport(transports);
}

public static Transport[] getTransports() {
	return transports;
	// TODO return required value
}

public static void addTransport(int i) {
	transports = new Transport[i];
	// TODO create new array of transports in size of passed value
}

public static void setVehicle(Transport transport, int i) {
	transports = new Transport[i];
	for (int j = 0; j<transports.length; j++){
		transports[j] = transport;
//		 transports[i] = transport;
	}
	// TODO set passed transport into transports array cell of passed index
}

public static void addRoads(int i) {
	roads = new Road[i];
	// TODO create new array of roads in size of passed value
}

public static Road[] getRoads() {
	// TODO return required value
	return roads;
}

public static void setRoad(Road road, int i) {
	// TODO set passed road into passed cell of roads array
	roads = new Road[i];
	for (int k = 0; k<roads.length; k++){
		roads[k] = road;

}

}}