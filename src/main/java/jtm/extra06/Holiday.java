package jtm.extra06;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.google.protobuf.TextFormat.ParseException;

/**
 * This enum represents holidays, displayed as month + day value. This enum can
 * give nearest holiday.
 */
public enum Holiday {
	NEW_YEAR(1, 1), WOMAN_DAY(3, 8), CHUCK_NORRIS_BIRTHSDAY(3, 10), FOOLS_DAY9(4, 1), WORLD_END(12, 21);
	int month;
	int day;

	Holiday(int month, int day) {
		this.month = month;
		this.day = day;
        Holiday holiday;
				
//		// TODO #1 implement class variables for month and day of the holiday

	}
	public static Holiday getNearest(int currentMonth, int currentDay) {
	   Holiday returnHoliday = null;
		
		switch (currentMonth) {
		case 1:
			returnHoliday = Holiday.NEW_YEAR;
			break;
		case 2:  
			returnHoliday = Holiday.WOMAN_DAY;
			break;
		case 3:
			returnHoliday = Holiday.CHUCK_NORRIS_BIRTHSDAY;
			break;
		case 4:
			returnHoliday = Holiday.FOOLS_DAY9;	
			break;
		case 5:
			returnHoliday = Holiday.WORLD_END;	
			break;
		}

		return returnHoliday;
	}
		   public static void main(String args[]) {
			   
	      Holiday holiday[] = Holiday.values();
	      for(Holiday hol: holiday) {
	System.out.println("The nearest Holiday is: "+ getNearest(1, 2));
	break;
	}}

// second attempt: :) I am not sure, but some ideas I think are correct
		   
    public static void main1(String args[]) throws AssertionError, ParseException, java.text.ParseException {

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        //comparing dates in java using Calendar.before(), Calendar.after and Calendar.equals()
        System.out.println("Comparing two Date in Java using Calendar's before, after and equals method");
        compareDatesByCalendarMethods(df, df.parse("19-09-2020"), df.parse("01-01-2021")); {
            System.out.println("New Year soon!");
        }
        compareDatesByCalendarMethods(df, df.parse("01-01-2021"), df.parse("03-08-2021"));
        compareDatesByCalendarMethods(df, df.parse("19-09-2020"), df.parse("03-08-2021"));
        compareDatesByCalendarMethods(df, df.parse("03-08-2021"), df.parse("10-08-2021"));
        compareDatesByCalendarMethods(df, df.parse("19-09-2020"), df.parse("10-03-2021"));
        compareDatesByCalendarMethods(df, df.parse("10-03-2021"), df.parse("01-04-2021"));
        compareDatesByCalendarMethods(df, df.parse("19-09-2020"), df.parse("01-04-2021"));
        compareDatesByCalendarMethods(df, df.parse("01-04-2021"), df.parse("21-12-2021"));
        compareDatesByCalendarMethods(df, df.parse("19-09-2020"), df.parse("21-12-2021"));
    }

    private static void compareDatesByCalendarMethods(DateFormat df, java.util.Date date, java.util.Date date2) {

        //how to check if date1 is equal to date2
        if (date.compareTo(date2) == 0) {
            System.out.println(df.format(date) + " and " + df.format(date2) + " are equal to each other");
        }

        //checking if date1 is less than date 2
        if (date.compareTo(date2) < 0) {
            System.out.println(df.format(date) + " is less than " + df.format(date2));
        }

        //how to check if date1 is greater than date2 in java
        if (date.compareTo(date2) > 0) {
            System.out.println(df.format(date) + " is greater than " + df.format(date2));
        }
    }
}


	  
		// TODO #2 implement method which will return the nearest holiday.
		// HINT: note, that holidays is arranged by date ascending, so if there
		// are
		// no more holidays this year, first holiday in the list will be the
		// next.
		

		

