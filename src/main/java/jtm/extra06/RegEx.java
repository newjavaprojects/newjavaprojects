package jtm.extra06;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {

	/**
	 * This method finds out if we can make lucky number from numbers in input
	 * string. Lucky number is number with digit sum equal to 25
	 * 
	 * @param string
	 *            , needed to be checked
	 * @return true if numbers in this number are lucky, false if not.
	 */
	public boolean isLuckyNumber(String input) {

		// TODO #1 Remove all non digits from the input.
		// HINT: use negation pattern.
        int sum = 0;
        String myString = "12286 text var25 14 85v 1";
        myString = myString.replaceAll("([0-9]*[^0-9\\s]+[0-9]*\\s*)", "");
        System.out.println(myString);
        for (int i = 0; i < myString.length(); i++) {
            char temp = myString.charAt(i);
            if (Character.isDigit(temp)) {
                int b = Integer.parseInt(String.valueOf(temp));
                sum = sum + b;
            }
        }
        System.out.println(sum);
        
        if (sum==25) {
        	System.out.println(sum + " is Lucky number!");
    		return true;
        }
        else {
        System.out.println(sum + " is not Lucky number!");
        return false;
        }

		// TODO #2 count the sum of all digits, and check if the sum is lucky
	
	
	}

	/**
	 * This method finds Kenny or Kelly hiding in this list. "Kenny" or "Kelly" can be written with
	 * arbitrary number of "n"s or "l"s starting with two.
	 * 
	 * @param input
	 *            — input string
	 * @return — position of "Kenny" string starting with zero. If there are no
	 *         "Ken..ny" return -1.
	 *         
	 *         
	 */
	

	public int findKenny(String input) {
        Pattern pattern = Pattern.compile("Ke(|n|nn|l|ll)y");

        // See if this String matches.
        Matcher m = pattern.matcher("Kenny");
        if (m.matches()) {
            System.out.println(true + " found Kenny");
     
        }else{
            System.out.println("not found Kenny");

        }

        // Check this String.
        m = pattern.matcher("Kelly");
        if (m.matches()) {
            System.out.println(true + " found Kelly");
        }else{
            System.out.println("not found Kelly");
        }
		return 1;
	}
	

	/**
	 * THis method checks if input string is correct telephone number. Correct
	 * Riga phone number starts with 67 or 66 and is followed by 6 other digits.
	 * not obligate prefix +371
	 * 
	 * @param telephoneNumber
	 *            - number, needed to be checked.
	 * @return true if number is valid Riga city number.
	 */
	public boolean isGood(String telephoneNumber) {
		
	       telephoneNumber = "67354900";

	       Pattern pattern = Pattern.compile("^((\\+371|00371|66|67)+([0-9]){6})$");
	       Matcher matcher = pattern.matcher(telephoneNumber);

	       if (matcher.matches()) {
	           System.out.println(telephoneNumber + " valid number is valid Riga city number");
	       }
	       else
	       {	    	   
	           System.out.println(telephoneNumber + " Not valid number, needed to be checked");

	       }
	       return true;
		// TODO #5 check with "matches" method if this number is valid.
		
	}
}
