package jtm.activity13;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TeacherManager {

	protected Connection conn;

	public TeacherManager()throws SQLException, ClassNotFoundException {
		
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// url =
		// "jdbc:mysql://127.0.0.1/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		// user = "student"
		// pass = "Student007"
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods
		
		    conn = null;
			Class.forName("com.mysql.cj.jdbc.Driver");

			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/database_activity", "root", "");
			conn.setAutoCommit(false);
//
			Statement stmt = conn.createStatement();
			ResultSet rs=stmt.executeQuery("SELECT * FROM database_activity.teacher");  
			List<Teacher> teachers = new ArrayList<Teacher>();
			while(rs.next()) {
				Teacher teacher = new Teacher (rs.getInt("id"), rs.getString("firstName"), rs.getString("lastName"));
				teachers.add(teacher);
			}
}

	
	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 */
	public Teacher findTeacher(int id) throws SQLException {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.Teacher"
		
		Teacher teacher = new Teacher(0, null, null);
		String selectSQL = "SELECT * FROM database_activity.teacher WHERE id= \"" + id + "\";" ;
		PreparedStatement statement = conn.prepareStatement(selectSQL);
		//statement.setString(1, String.valueOf(id));
		ResultSet rs = statement.executeQuery(selectSQL);
		if (rs.next())
				teacher = new Teacher(rs.getInt("id"), rs.getString("firstname"), 
						rs.getString("lastname"));
		return teacher;		
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		
		List<Teacher> list = new ArrayList<>();
		Teacher teacher = new Teacher();	
		try{
		String sql = "SELECT * FROM database_activity.teacher WHERE firstname LIKE ? and lastname LIKE ?";
		PreparedStatement preparedStatement = conn.prepareStatement(sql);

		preparedStatement.setString(1, "%" + firstName + "%");  
		preparedStatement.setString(2, "%" + lastName + "%"); 
		ResultSet rs = preparedStatement.executeQuery();
		conn.commit();
		while (rs.next()) {
			teacher = new Teacher(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"));
            list.add(teacher);
    }
		}
          catch (SQLException e) 
            { e.printStackTrace();}
		return list;
		}
	
	
	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 * @throws SQLException 
	 */

	public boolean insertTeacher(String firstName, String lastName) throws SQLException  {
		//boolean status = false;
		// TODO #4 Write an sql statement that inserts teacher in database.
		try {
		String sql = "INSERT INTO database_activity.Teacher (firstname, lastname) VALUES (?, ?)";
		PreparedStatement preparedStatement = conn.prepareStatement(sql);
		preparedStatement.setString(1, firstName);
		preparedStatement.setString(2, lastName);
		int count = preparedStatement.executeUpdate();
		
		if (count > 0) {
		conn.commit();
		return true;
		}
		else
			return false;
		}
		catch (SQLException e) {
			return false;
		}
//		else {
//			conn.rollback();
//			return false;
	}
	
	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 * @throws SQLException 
	 */
	public boolean insertTeacher(Teacher teacher) {

	try {
		String sql = "INSERT INTO database_activity.teacher (id, firstname, lastname) VALUES (?, ?, ?)";
		PreparedStatement preparedStatement = conn.prepareStatement(sql);
		preparedStatement.setInt(1, teacher.getId());
		preparedStatement.setString(2, teacher.getFirstName());
		preparedStatement.setString(2, teacher.getLastName());
		//preparedStatement.executeUpdate(sql);
		int rows = preparedStatement.executeUpdate();
		
		if (rows > 0) {
		conn.commit();
		return true;
		} else
			return false;
	}catch (SQLException e) {
			System.err.println(e);
//		else {
//			conn.rollback();
			return false;
		}
	}


		
		// TODO #5 Write an sql statement that inserts teacher in database.

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 */
	public boolean updateTeacher(Teacher teacher) {
		boolean status = false;
		try{
		String sql = "UPDATE database_activity.teacher set firstname = ?, lastname = ? where id = ?";
		PreparedStatement preparedStatement = conn.prepareStatement(sql);
		preparedStatement.setInt(3, teacher.getId());
		preparedStatement.setString(1, teacher.getFirstName());
		preparedStatement.setString(2, teacher.getLastName());
		if (preparedStatement.executeUpdate() > 0) {
			conn.commit();
			return true;
		} else
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
		//conn.setAutoCommit(true);
		//return status;
		// TODO #6 Write an sql statement that updates teacher information.

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 * @throws SQLException 
	 */
	public boolean deleteTeacher(int id) {
		// TODO #7 Write an sql statement that deletes teacher from database.
		try {
		PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM database_activity.teacher WHERE id = ?");
		preparedStatement.setInt(1, id);
//		boolean success = preparedStatement.execute(sql);
//		conn.setAutoCommit(true);
		preparedStatement.executeUpdate();
		conn.commit();
//		if (rowDeleted > 0) {
		return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
		//return false;
	}
	public void closeConnecion() {
		// TODO Close connection to the database server and reset conn object to null
		try {
	if (conn != null)
		conn.close();
		conn= null;
	} catch (SQLException e) {
		e.printStackTrace();
	}
	}

}
