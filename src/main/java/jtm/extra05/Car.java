package jtm.extra05;

import java.util.List;

public class Car {

	private String model;
	private Integer year;
	private String color;
	private Float price;
	
	// TODO #1 generate public constructor which takes all properties of an
	// object as parameters

public Car(String model, Integer year, String color, Float price) {
	super();
	this.model = model;
	this.year = year;
	this.color = color;
	this.price = price;

	// TODO #2 generate public getters of all object properties
}

/**
 * @return the model
 */
public String getModel() {
	return model;
}

/**
 * @param model the model to set
 */
public void setModel(String model) {
	this.model = model;
}

/**
 * @return the year
 */
public Integer getYear() {
	return year;
}

/**
 * @param year the year to set
 */
public void setYear(Integer year) {
	this.year = year;
}

/**
 * @return the color
 */
public String getColor() {
	return color;
}

/**
 * @param color the color to set
 */
public void setColor(String color) {
	this.color = color;
}

/**
 * @return the price
 */
public Float getPrice() {
	return price;
}

/**
 * @param price the price to set
 */
public void setPrice(Float price) {
	this.price = price;
}

@Override
public String toString() {
    return "Car model=" + model + ", year=" + year + ", color=" + color
            + ", price=" + price + ".";

}

public void setColor(List<String> color2) {
	// TODO Auto-generated method stub
	
}

}
