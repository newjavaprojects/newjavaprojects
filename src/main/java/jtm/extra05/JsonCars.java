package jtm.extra05;
import java.util.ArrayList;
import java.util.List;
import org.json.*;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonCars {
	 
    private List<Car> cars = new ArrayList<Car>();
	public List<Car> getCars() {
        return cars;
    }
    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "JsonCars [cars=" + cars + "]";
    }

	/*- TODO #1
	 * Implement method, which returns list of cars from generated JSON string
	 */
	public List<Car> getCars(String jsonString) {

	      JSONObject car1 = new JSONObject();
	      try {
	         car1.put("Model", "BMW");
	         car1.put("Year", 2020);
	         car1.put("Color", "White");
	         car1.put("Price", 22000f);

	      } catch (JSONException e) {
	          // TODO Auto-generated catch block
	          e.printStackTrace();
	      }

	      JSONObject car2 = new JSONObject();
	      try {
	          car2.put("Model", "Bentley");
		      car2.put("Year", 2020);
		      car2.put("Color", "red");
		      car2.put("Price", 220000f);

	      } catch (JSONException e) {
	          // TODO Auto-generated catch block
	          e.printStackTrace();
	      }

	      JSONArray jsonArray = new JSONArray();

	      jsonArray.put(car1);
	      jsonArray.put(car2);

	      JSONObject carObj = new JSONObject();
	      carObj.put("Cars", jsonArray);

	      String jsonStr = carObj.toString();

	       System.out.println("jsonString: "+jsonStr);

			return cars;
	      }

		/*- HINTS:
		 * You will need to use:
		 * - https://stleary.github.io/JSON-java/org/json/JSONObject.html
		 * - https://stleary.github.io/JSON-java/org/json/JSONArray.html
		 * You will need to initialize JSON array from "cars" key in JSON string
		 */

	/*- TODO #2	
	 * Implement method, which returns JSON String generated from list of cars
	 */
	public String getJson(List<Car> cars) {

		/*- HINTS:
		 * You will need to use:
		 * - https://docs.oracle.com/javase/8/docs/api/index.html?java/io/StringWriter.html
		 * - http://static.javadoc.io/org.json/json/20180130/index.html?org/json/JSONWriter.html
		 * Remember to add "car" key as a single container for array of car objects in it.
		 */
		
		String jsonStr= "{\"Cars\":[{\"Year\":2020,\"Price\":22000,\"Color\":\"White\",\"Model\":\"BMW\"},{\"Year\":2020,\"Price\":220000,\"Color\":\"red\",\"Model\":\"Bentley\"}]}\n" + 
				"";
	      ObjectMapper objectMapper = new ObjectMapper();
	      try {
	          JsonCars cars1 = objectMapper.readValue(jsonStr, JsonCars.class);
	         System.out.println("The cars are:\n " + cars1);
	      } catch(Exception e) {
	         e.printStackTrace();
	      }
		return null;
	}	}
	
